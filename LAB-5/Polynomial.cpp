//***************************************************************
//    	HEADER FILES
//****************************************************************                       Run in Code::Blocks or Dec-C++

#include<iostream>
#include<stdlib.h>
#include<conio.h>
					      																		  
using namespace std;

//***************************************************************
//					CLASS PLOYNOMIAL
//****************************************************************

class Polynomial
{
	int *array1;													//For co-efficient of the terms			Pointer to int
	int *array2;													//For degree of the terms
	static int n;
	static int a;
	
  public:
   	
   	Polynomial(int x)												//Constructors
	{	n=x;
	 	a=x;
		array1 = new int[a+1];
		array2 = new int[a+1];
	}

	Polynomial()
	{ }									
   										
   	~Polynomial()													//Destructor
	{	delete array1;
		delete array2;
	}
   	
   	friend ostream &operator<<( ostream &output, const Polynomial &P )
    { 	
    	cout<<"\n Polynomial:";
        for(int i=0;i< a+1 ;i++)
        {
         	output<<"(";	
			output << P.array1[i] ;
			output<<"x^";
			output << P.array2[i] ;
			output<<")";
         	if(i< a )
        		output << "+";
        }
        return output;
    }
     
    friend istream &operator>>( istream  &input, Polynomial &P )
    { 
        cout<<"\n Enter Coff:";
		for(int i=0;i<n+1;i++)
        	input >> P.array1[i];
		cout<<"\n\n Enter Power dec Order";
		for(int i=0;i<n+1;i++)
        	input >> P.array1[i];
        return input;            
	}

	Polynomial operator +(Polynomial &p)
	{	Polynomial re;
		for(int i=0;i<a+1;i++)
			if(p.array2[i]==array2[i])
				re.array1[i]=p.array1[i]+array1[i];
		cout<<"\n Sum:";
		for(int j=0;j<a+1;j++)
		{
         	cout <<"(" << re.array1[j] << "x^" << p.array2[j] << ")" ;
         	if(j<a)
        		cout << "+";
    	}	
	}
	
	Polynomial operator -(Polynomial p)
	{	Polynomial re;
		for(int i=0;i<n+1;i++)
			if(p.array2[i]==array2[i])
				re.array1[i]=array1[i]-p.array1[i];
		cout<<"\n Subtraction";
		for(int j=0;j<n+1;j++)
		 {
         	cout <<"(" << re.array1[j] << "x^" << array2[j] << ")" ;
         	if(j<n)
        		cout << "+";
    	}	
	}
	
	Polynomial operator *(Polynomial p)
	{	Polynomial re;
		int k=0;
			for(int i=0;i<n+1;i++)
			for(int j=0;j<n;j++)
			{	re.array1[k]=p.array1[i]*array2[j];
				re.array2[k]=p.array2[i]+array2[j];
				k++;
			}
		cout<<"\n Multiplication";
		for(int j=0;j<k;j++)
		 {
         	cout <<"(" << re.array1[j] << "x^" << re.array2[j] << ")" ;
         	if(j<k-1)
        		cout << "+";
    	}
		
	}
	   	
};

//***************************************************************
//    	THE MAIN FUNCTION OF PROGRAM
//****************************************************************

int main()
{
	int ch,n;
	cout<<"\n Enter The Highest Degree Of The Polynomial:";
	cin>>n;
	Polynomial poly(n);
	cin>>poly;
	Polynomial poly2=poly;
	Polynomial poly3(n);
		
    do
	{   system("cls");
		cout<<"\n\n\tMAIN MENU";
		cout<<"\n\n\t 1. Add";
		cout<<"\n\n\t 2. Subtract";
		cout<<"\n\n\t 3. Multiply";
		cout<<"\n\n\t 4. Display";
		cout<<"\n\n\t 5. Exit";
		cout<<"\n\n\tPlease Select Your Option (1-5) ";
		cin>>ch;
		switch(ch)
		{	case 1:
				poly3=poly+poly2;
				break;
			case 2:
				poly3=poly-poly2;
				break;
			case 3:
				poly3=poly*poly2;
				break;
			case 4:
				cout<<poly;
				break;
			case 5:
				exit(0);
		}
	}while(ch!='5');
	return 0;
}
