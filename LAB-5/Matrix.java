import java.util.Scanner;

//class Mat starts
class Mat
{	int[][] array;
	Scanner in =new Scanner(System.in); 
	int row, col;

    public

	//Normal Constructor	 
	Mat(int m, int n)					
	{	row=m;
		col=n;
		array=new int[row][col];
		for(int i=0; i<row;i++)
			for(int j=0;j<col;j++)
				array[i][j]=0;
	}

        //Funtion for assigning value to an array
	void assign(int m,int n)					
	{	int a;
		System.out.println("\nEnter Matrix");
		for(int i=0; i<row;i++)
			for(int j=0;j<col;j++)
			{	a=in.nextInt();
				array[i][j]=a;
			}
	}

        //Copy Contructor   
	Mat(Mat m)						
	{	row=m.row;
		col=m.col;
		array=new int[row][col];
		for(int i=0; i<row;i++)
			for(int j=0;j<col;j++)
				array[i][j]=m.array[i][j];
	}

        //Function for extracting the value from an array
	void extract()						
	{	int a;	
		System.out.println("\nOutput Matrix");
		for(int i=0; i<row;i++)
		{
			System.out.println(" ");
			for(int j=0;j<col;j++)
			{	a=array[i][j];
				System.out.print(a+" ");
			}
		}
	}

        //Funtion for transpose of matrix
	void transpose()						
	{	System.out.println("\nTranspose Of Matrix");
		for(int i=0; i<row;i++)
			for(int j=0;j<col;j++)
			{	int a=0;
				a=array[j][i];
				System.out.println(a+" ");
			}
	}

        //Funtion for sum of two matrices
	void sum(Mat m)						
	{	if((row==m.row)&&(col==m.col))                              //if statement for checking whether the two matrices can be added or not
		{	System.out.println("\nMatrices Can Be Added\n");	
			System.out.println("\nSum Of Matrices");
			for(int i=0; i<row;i++)
				for(int j=0;j<col;j++)
				{	int a=0;
					a=array[i][j]+m.array[i][j];        //Each element of a matrix is added with the respective element of other matrix
					System.out.println(a+" ");
				}
		}
		else                                                        
			System.out.println("\nMatrices Cannot be Added");
	}

        //Function for multiplication of two matrices
	void multiplication(Mat m)					
	{       //two matices can be mutltiplied only when rows of the first matrix and columns of the other matrix are equal
        	if(col==m.row)                                                     
		{	System.out.println("\nMatrices Can Be Multiplied\n");	
			System.out.println("\nMultiplication Of Matrices");
			for(int i=0; i<row;i++)
				for(int j=0;j<m.col;j++)
				{	int a=0;
					for(int k=0;k<col;k++)
						a=a+(array[i][k]*m.array[k][j]); 
					System.out.println(a+" ");
				}
		}
		else 
			System.out.println("\nMatrices Cannot be Multiplied");
	}

        //Function for multiplication of a matrix with a scalar
	void scalar()						
	{	System.out.println("\nEnter A Number");
		int ch1;
		ch1=in.nextInt();
		for(int i=0; i<row;i++)
			for(int j=0;j<col;j++)
			{	int a;
				a=ch1*array[i][j];
				System.out.println(a+" ");
			}
	}
	
}//class Mat ends

//class Matrix starts
class Matrix
{       //main function starts
	public static void main(String[] args)
	{	Scanner in =new Scanner(System.in); 
		int m,n,choice;		
		
		System.out.println("\nAssign values to a matrix");
		System.out.println("\nInput Row");
 
		m=in.nextInt();
		System.out.println("\nInput Column");
		n=in.nextInt();
		Mat m1=new Mat(m,n);
		m1.assign(m,n);						//Normal Contructor is called

		Mat m2=new Mat(m1);					//Copy Contructor is called
	
		do{
	 			System.out.println("\n\t1.Extract values from a matrix");
				System.out.println("\n\t2.Find transpose of a matrix");
				System.out.println("\n\t3.Add two matrices");
				System.out.println("\n\t4.Multiply two matrices");
				System.out.println("\n\t5.Multiply a matrix with a scalar value");
				System.out.println("\n\t6.Exit");
	 			System.out.println("\n\tEnter Your Choice:");
	 			choice=in.nextInt();
	 			switch(choice)
				{
					case 1:			
						m1.extract();             //Function for extracting the value is called
						break;
					case 2:			
						m1.transpose();           //Function for transpose of a matix is called
						break;
					case 3:			
						m1.sum(m2);               //Function for sum of a matrix is called
						break;
					case 4:			
						m1.multiplication(m2);    //Funtion for multiplication is called
						break;
					case 5:			
						m1.scalar();              //Function for multiplication with a scalar is called  
						break;
					
				}

	 		}while(choice<6);
	}//main function ends
}
//program ends
