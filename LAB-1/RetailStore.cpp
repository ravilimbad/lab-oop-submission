   //////////////////////////////////////////////////////////////////////////////////////////////////
  //                                     MADE BY:- RAVI LIMBAD                                    //
 //                                        ROLL NO:- 1401077                                     // 
//////////////////////////////////////////////////////////////////////////////////////////////////


  //////////////////////////////////////////////////////////////////////////////////////////////////
 //                                          HEADER FILE                                         //
//////////////////////////////////////////////////////////////////////////////////////////////////

#include<iostream>
#include<conio.h>
using namespace std;

  //////////////////////////////////////////////////////////////////////////////////////////////////
 //                                  STRUCTURE FOR PRODUCT                                       //
//////////////////////////////////////////////////////////////////////////////////////////////////

struct product
{
	char productname[20];                              // NAME OF THE PRODUCT PURCHASED
	int quantity;                                      // QUANTITY OF THE PRODUCT PURCHASED  
	float price;                                       // PRICE OF THE PURCHASED PRODUCT
};

  //////////////////////////////////////////////////////////////////////////////////////////////////
 //                                  STRUCTURE FOR CUSTOMER                                      //
//////////////////////////////////////////////////////////////////////////////////////////////////

struct cust
{
	char name[20];                                     // NAME OF THE CUSTOMER
    unsigned long long int phoneno;                    // PHONE NUMBER OF THE CUSTOMER
	product detail[20];
	float bill;                                 
} c1;

  //////////////////////////////////////////////////////////////////////////////////////////////////
 //                             FUNCTION FOR CALCULATING BILL                                    //
//////////////////////////////////////////////////////////////////////////////////////////////////

float cal(cust c1)
{

	float n=0.0;
	int i;
	for(i=1;i<=c1.detail[i].quantity;i++)
	{
		n=n+(c1.detail[i].price*c1.detail[i].quantity);
	}
	return n;
};

  //////////////////////////////////////////////////////////////////////////////////////////////////
 //                                        MAIN FUNCTION                                         //
//////////////////////////////////////////////////////////////////////////////////////////////////

int main()
{
    cust c1;
	int n, i;
	cout<<"\n\t\t\tEnter Customer details : ";
	cout<<"\nEnter Customer name : ";
	cin>>c1.name;
	cout<<"\nEnter Customer Phone Number : ";
	cin>>c1.phoneno;
	cout<<"\n\n\t\t\tEnter product details : ";
	cout<<"\nEnter number of products purchased : ";
	cin>>n;
	for(i=1; i<=n; i++)
	{
		cout<<"\nEnter product name : ";
		cin>>c1.detail[i].productname;
		cout<<"\nEnter quantity : ";
		cin>>c1.detail[i].quantity;
		cout<<"\nEnter price : ";
		cin>>c1.detail[i].price;
	}
	c1.bill=cal(c1);


	cout<<"\n\n....................................BILL...................................";
	cout<<"\n\nCUSTOMER NAME : "<<c1.name;
	cout<<"\n\nCUSTOMER PHONE NUMBER : "<<c1.phoneno;
	cout<<"\n\n\t\t\tCASH MEMO : ";
	cout<<"\nSr.No\tProduct name\t\t\tQuantity\tPrice";
	for(i=1;i<=n;i++)
	{
		cout<<"\n"<<i<<")\t"<<c1.detail[i].productname<<"\t\t\t\t"<<c1.detail[i].quantity<<"\t\t"<<c1.detail[i].price;
	}
	cout<<"\n\n\t\t\t\t\t Total Amount : "<<c1.bill;
	getch();
	return 0;
}

  //////////////////////////////////////////////////////////////////////////////////////////////////
 //                                     END OF THE PROGRAM                                       //
//////////////////////////////////////////////////////////////////////////////////////////////////

     ///////////////////////////////////////////////////////////////////////////////////////////////
    //                                           REFERENCE                                       //
   //               --> Programming in ANSIC by E Balagurusamy Fourth Edition                   //
  //                --> Deitel  and Deitel, C How to Program                                   //
 //                 --> Dey and Ghosh, Programming in C, Oxford University Press              // 
///////////////////////////////////////////////////////////////////////////////////////////////


