/*Program starts here*/

import java.util.Scanner;
import static java.lang.Math.pow;           /*This statement is written because we have used power function for calculating interest*/                                

/*Account class begins here which is abstract class*/
abstract class Account	 								
{	int acc_no, month, year;				        /*data members for account number, month and year respectively*/
	float balance, withdraw, credit, interest;			/*data members for balance amount, withdraw amount, credit amount and interest*/
	String name;							/*data member for storing User's name*/

	Scanner input = new Scanner(System.in);
	
  public
  	
  	Account()							/*Constructor for Account class*/
   	{	withdraw=0;						/*initializing withdraw amount to 0*/
  		credit=0;						/*initializing credit/deposit amount to 0*/
  		name="Null";						/*initializing User's name to NULL*/
		month=4;						/*initializing month to 4*/
		year=2015;						/*initializing year to 2015*/
		interest=0;
  	}
  	
  	abstract void withdraw();       /* Function for withdrawing amount which is declared as abstract because both the sub classes have different logic for
                                           withdrawing the amount*/   
	abstract void create();         /* Function for creating a new account which is declares as abstract because both the sub classes have different initial      
  	  	                           conditions to create an account*/
  	
        void credit()                                                       /*Function for crediting amount in the account*/ 
  	{	System.out.println("\nEnter the amount to Credit:");
  		credit=input.nextFloat();
  		balance+=credit;                                            /*balance = balance + credit*/
  		System.out.println("\nYour current balance is:"+balance);   /*Displays the balance*/
 	}
 	
 	void display()                                                      /*Funtion for displaying the account details*/ 
 	{	System.out.println("\nName:"+name);
 		System.out.println("Account Number:"+acc_no);
 		System.out.println("Your current balance is:"+balance);
 	}

	void interest()                                                     /*Function for calculating interest*/
   	{	int m, y, t, mo;                                            /*here m=Current month, y=current year, t and mo are temporary variables*/   
		float f, p;                                                 /*f and p are temporary variables*/
		System.out.println("\nEnter Current Month Number:");
  		m=input.nextInt();                                          /*taking input of current month from the user*/
  		System.out.println("\nEnter Current Year:");
  		y=input.nextInt();                                          /*taking input of current year from the user*/
                
                /*here y-year means current year - year in which account was created * 12 then + m(i.e. current month) the - month(i.e. month in which account was 
                  created)*/  
		mo=(((y-year)*12)+m)-month;
		t=mo/12;
		p=(float)pow((1+(0.1/3)),3*t);
		f=balance*p;
		interest=f-balance;
		balance=f;
   		System.out.println("\nInterest got after "+mo+" months:"+interest); /*Displays how much interest the user will get after how many months*/ 
   		System.out.println("\nYour current balance is:"+balance);           /*Displays the balance after adding interest's amount*/
   	}
 	
};/*Account class ends here*/
 
/*Savings class which is inherited from the account class(i.e. Sud-class of Account class) begins here*/
class Savings extends Account
{	static int sacc_no=10;             /*data member sacc_no is savings account no which is declared as static(Because later on we don't have to specify the 
                                             instance of a class) and initialized to 10*/
                                                     
	Scanner input=new Scanner(System.in);
	
  public
   	
   	Savings()                                             /*Constructor fo Savings class*/
   	{	balance=1000;                                 /*initializing balance = 1000 because user can create savings account with a minimum balance of 1000*/  
		acc_no=sacc_no;
   	}

	void create()                                               /*Function for creating savings account*/ 
  	{	sacc_no++;
		acc_no=sacc_no;
  		System.out.println("\nAccount number:"+sacc_no);    /*Displays the savings account number*/		
  		System.out.println("\nEnter Name:");                
  		name=input.nextLine();                              /*taking input from the user of his/her name*/ 
  	}   	

   	void withdraw()                                                         /*Funtion for withdrawing money from the savings account*/ 
  	{	System.out.println("\nEnter the amount to Withdraw:");
	  	withdraw=input.nextFloat();                                     /*taking input from the user of the amount he/she wants to withdraw*/

                /*if condition is used to check whether the remaining balance after withdrawing is >=500 or not because user have to keep a minimum balance of 500 
                  after withdrawing certain amount */
  		if(withdraw <= balance-500)                                          
  		{	balance-=withdraw;
	  		System.out.println("\nYour current balance is:"+balance);   /*Displays the remaining balance*/
	  	}

                /*if the remaining balance will become <500 then the user cannot withdraw the amount which he/she has entered. The user have to withdraw lesser 
                  amount */
	  	else
	  		System.out.println("\nSORRY CAN NOT WITHDRAW.Your current balance is:"+balance+" Cannot withdraw more than:"+(balance-500));
	}
	 
};/*Savings class ends here*/

/*Current class which is also inherited from the account class(i.e. Sud-class of Account class) begins here*/
class Current extends Account
{	float overdraft;                     /*data member for overdraft*/
	static int cacc_no=20;               /*data member cacc_no is current account no which is declared as static(Because later on we don't have to specify the 
                                               instance of a class) and initialized to 20*/           
	
	Scanner input=new Scanner(System.in);
	
  public
   	
   	Current()                                                 /*Constructor for Current class*/
   	{	balance=0;                                        /*initializing balance = 0 because user can create current account with 0 balance also*/
		acc_no=cacc_no;
		overdraft=0;                                      /*initializing overdraft to 0*/
	}

	void create()                                             /*Function for creating current account*/
  	{	cacc_no++;
		acc_no=cacc_no;
  		System.out.println("\nAccount number:"+cacc_no);  /*Displays the current account number*/	
  		System.out.println("\nEnter Name:");
  		name=input.nextLine();                            /*taking input from the user of his/her name*/ 
		do{
			if(overdraft<500 && overdraft>10000000)
				System.out.println("\nEnter overdraft limit[ranging from Rs.500/- to Rs.1 Cr]:");
			else
				System.out.println("\nEnter overdraft limit[ranging from Rs.500/- to Rs.1 Cr]:");
			overdraft=input.nextFloat();
		}while(overdraft<500 && overdraft>10000000);
  	}   	
   	   	
   	void withdraw()                                                             /*Funtion for withdrawing money from the savings account*/ 
  	{	System.out.println("\nEnter the amount to Withdraw:");
	  	withdraw=input.nextFloat();                                         /*taking input from the user of the amount he/she wants to withdraw*/
		if(withdraw <= balance+overdraft)
  		{	balance-=withdraw;
	  		System.out.println("\nYour current balance is:"+balance);   /*Displays the remaining balance*/
	  	}
	  	else
	  		System.out.println("\nSORRY CAN NOT WITHDRAW.Your current balance is:"+balance+" Cannot withdraw more than:"+(balance+overdraft));
	 }
	 
};/*Current class ends here*/

/*Banks class which contains main funtion begins here*/
class Banks
{       /*main funtion begins here*/
	public static void main(String [] args)
	{	Scanner input=new Scanner(System.in);
	
		int choice,choice1, sa=0, cu=0, account_no, i, j;        /*data members of Banks class*/

		Account []sobj, cobj;                                    /*reference variables for the objects of Sub-Classes Savings and Current*/
		sobj=new Savings[10];                                    /*Creating array of objects for Savings class*/ 
		cobj=new Current[10];                                    /*Creating array of objects for Current class*/
				
		do{
			System.out.println("\n\n1.Create Account");
			System.out.println("\n2.Deposit Amount");
			System.out.println("\n3.Withdraw Amount");
			System.out.println("\n4.Display Account");
			System.out.println("\n5.Intreset Cal On Saving Account");
			System.out.println("\n6.Exit");
			choice=input.nextInt();                                        /*Taking input from the user for the choice feom the menu*/
			switch(choice)                
			{	case 1:
					System.out.println("\n\n1.Saving Account");
					System.out.println("\n2.Current Account");
					System.out.println("\nEnter your choice:");
					choice1=input.nextInt();                       /*Taking input from the user for the choice feom the menu*/
					if(choice1==1)
					{	sobj[sa]=new Savings();                /*initializing */
						sobj[sa].create();                     /*Funtion for creating savings account will be called*/
						sa++;
					}
					else if(choice1==2)
					{	cobj[cu]=new Current();
						cobj[cu].create();                     /*Funtion for creating current account will be called*/
						cu++;
					}
					break;
				case 2:
					System.out.println("\nEnter Account number:");
					account_no=input.nextInt();                    /*taking input of account number from the user*/ 
					i=account_no/10;
					if(i==1)
					{	j=account_no%10;
						if(j>=sa)
						{	j--;
							sobj[j].credit();              /*Credit function will be called here*/
						}
						else
							System.out.println("\nNo Such Account Number Exist...");
					}
					else if(i==2)
					{	j=account_no%10;
						if(j>=cu)
						{	j--;
							cobj[j].credit();              /*Credit function will be called here*/ 
						}
						else
							System.out.println("\nNo Such Account Number Exist...");
					}
					break;
				case 3:
					System.out.println("\nEnter Account number:");
					account_no=input.nextInt();                    /*taking input of account number from the user*/
					i=account_no/10;
					if(i==1)
					{	j=account_no%10;
						if(j>=sa)
						{	j--;
							sobj[j].withdraw();            /*Withdraw function will be called here*/
						}
						else
							System.out.println("\nNo Such Account Number Exist...");
					}
					else if(i==2)
					{	j=account_no%10;
						if(j>=cu)
						{	j--;
							cobj[j].withdraw();            /*Withdraw function will be called here*/ 
						}
						else
							System.out.println("\nNo Such Account Number Exist...");
					}
					break;
				case 4:
					System.out.println("\nEnter Account number:");
					account_no=input.nextInt();                    /*taking input of account number from the user*/
					i=account_no/10;
					if(i==1)
					{	j=account_no%10;
						if(j>=sa)
						{	j--;
							sobj[j].display();             /*Display function will be called here*/
						}
						else
							System.out.println("\nNo Such Account Number Exist...");
					}
					else if(i==2)
					{	j=account_no%10;
						if(j>=cu)
						{	j--;
							cobj[j].display();             /*Display function will be called here*/
						}
						else
							System.out.println("\nNo Such Account Number Exist...");
					}
					break;
				case 5:
					System.out.println("\nEnter Account number:");
					account_no=input.nextInt();                    /*taking input of account number from the user*/
					i=account_no/10;
					if(i==1)
					{	j=account_no%10;
						j--;
						sobj[j].interest();                    /*interest funtion will be called here*/
					}
					else if(i==2)
						System.out.println("\nNo Such Account Number Exist...");
			}
		}while(choice<6);
	}/*main funtion ends here*/
}/*Banks class ends here and program as well*/

/* Reference:-   --> www.stackoverflow.com 
                 --> The Complete Reference Java, Fifth Edition, Herbert Schildt
*/
