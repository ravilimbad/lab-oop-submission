
//Program starts here
import java.util.Scanner;

//class Media starts here
class Media
{	String title;											
	int price, yearOfPublication;           
 	
 	Scanner input = new Scanner(System.in);
 
  public
 	
 	Media()									//Normal Constructor of class Media
 	{	title=new String("NULL");
 		price=0;
 		yearOfPublication=0;
 	} 	
 	
 	void set_title()							//Function for setting title
 	{	System.out.print("Enter Title:");
 		title=input.nextLine();
 	}
 	
 	String get_title()							//Function for diplaying title		
 	{	return title;	}
 	
 	void set_price()							//Function for setting price
 	{	System.out.print("Enter Price:");	
 		price=input.nextInt();
 	}
 	
 	int get_price()                                                         //Function for diplaying price
 	{	return price; }
 	
 	void set_yearOfPublication()			      			//Function for setting year
 	{	System.out.print("Enter Year of Publication:");	
 		yearOfPublication=input.nextInt();
 	}
 	
 	int get_yearOfPublication()                                             //Function for diplaying yearOfPublication
 	{ 	return yearOfPublication;	}
 	
 }//class Media ends here
 
//class Book starts here
//class Book inherits class Media
 class Book extends Media								
 {	String author;
 	int no_of_pages;
 	
 	Scanner input = new Scanner(System.in);
 	
   public
   	
   	Book()									//Normal Constructor of class Book
   	{	author=new String("NULL");
   		no_of_pages=0;
   	}
   	
   	void set_author()							//Function for setting author
 	{	System.out.print("Enter Author:");
 		author=input.nextLine();
 	}
 	
 	String get_author()							//Function for displaying author	
 	{	return author;	}
 	
 	void set_no_of_pages()							//Function for setting no_of_pages
 	{	System.out.print("Enter Page Number:");	
 		no_of_pages=input.nextInt();
 	}
 	
 	int get_no_of_pages()				                        //Function for displaying no_of_pages
 	{	return no_of_pages; }
 	
 }//class Book ends here
 
//class CD starts here
//class CD inherits class Media
 class CD extends Media									
 {	float sizeInMB, time;
 
 	Scanner input = new Scanner(System.in);
 
   public
   	 
   	CD()									//Normal Constructor of class CD
   	{	sizeInMB=0;
   		time=0;
   	}
   	
   	void set_sizeInMB()							//Function for setting size of CD
 	{	System.out.print("Enter Size Of CD(in MB):");	
 		sizeInMB=input.nextFloat();
 	}
 	
 	float get_sizeInMB()                                                    //Function for displaying size of CD
 	{	return sizeInMB; }
 	
 	void set_time()								//Function for setting duration of CD
 	{	System.out.print("Enter Duration(in min):");	
 		time=input.nextFloat();
 	}
 	
 	float get_time()                                                        //Function for displaying duration of CD
 	{ 	return time;	}
 	
 }//class CD ends here
 
//class MyMedia starts
 class MyMedia
 {	//main function starts
        public static void main(String args[])
	{	Scanner input = new Scanner(System.in);
		
		int i,choice;	
		
		Book []b=new Book[3];						//array of objects of class Book
		for(i=0;i<3;i++)
			b[i]=new Book();
			
		CD []c=new CD[3];						//array of objects of class CD
		for(i=0;i<3;i++)
			c[i]=new CD();
	
		do{
	 			System.out.println("\n\t1.Set Info Of Book");
				System.out.println("\n\t2.Set Info Of CD");
				System.out.println("\n\t3.Get Info Of Book");
				System.out.println("\n\t4.Get Info Of CD");
				System.out.println("\n\t5.Exit");
	 			System.out.println("\n\tEnter Your Choice:");
	 			choice=input.nextInt();
	 			switch(choice)
				{
					case 1:		                	//Setting information of Book
						for(i=0;i<2;i++)
						{	System.out.print("\nEnter Book"+(i+1)+"\n");
							b[i].set_title();
							b[i].set_price();
							b[i].set_yearOfPublication();
							b[i].set_author();
							b[i].set_no_of_pages();
							System.out.println();
						}						
						break;
					case 2:		                	//Setting information of CD
						for(i=0;i<2;i++)
						{	System.out.print("\nEnter CD"+(i+1)+"\n");
							c[i].set_title();
							c[i].set_price();
							c[i].set_yearOfPublication();
							c[i].set_sizeInMB();
							c[i].set_time();
							System.out.println();
						}						
						break;
					case 3:		                	//Getting information of Book
						for(i=0;i<2;i++)
						{	System.out.print("\nBook"+(i+1));
							System.out.print("\nTitle:"+b[i].get_title());
							System.out.print("\nPrice:"+b[i].get_price());
							System.out.print("\nYear:"+b[i].get_yearOfPublication());
							System.out.print("\nAuthor:"+b[i].get_author());
							System.out.print("\nPage Number:"+b[i].get_no_of_pages());						
							System.out.println();
						}
						break;
					case 4:		                	//Getting information of CD
						for(i=0;i<2;i++)
						{	System.out.print("\nCD"+(i+1));
							System.out.print("\nTitle:"+c[i].get_title());
							System.out.print("\nPrice:"+c[i].get_price());
							System.out.print("\nYear:"+c[i].get_yearOfPublication());
							System.out.print("\nSize Of CD(in MB):"+c[i].get_sizeInMB());
							System.out.print("\nDuration(in min):"+c[i].get_time());
							System.out.println();							
						}
						break;
					
				}

	 		}while(choice<=4);
	}//main function ends here
}//MyMedia class and program ends here

