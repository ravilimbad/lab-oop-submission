   //////////////////////////////////////////////////////////////////////////////////////////////////
  //                                     MADE BY:- RAVI LIMBAD                                    //
 //                                        ROLL NO:- 1401077                                     //
//////////////////////////////////////////////////////////////////////////////////////////////////


  //////////////////////////////////////////////////////////////////////////////////////////////////
 //                                          HEADER FILE                                         //
//////////////////////////////////////////////////////////////////////////////////////////////////

#include<stdio.h>
#include<conio.h>

  //////////////////////////////////////////////////////////////////////////////////////////////////
 //                                  STRUCTURE FOR PRODUCT                                       //
//////////////////////////////////////////////////////////////////////////////////////////////////

struct product
{
	char productname[20];                              // NAME OF THE PRODUCT PURCHASED
	int quantity;                                      // QUANTITY OF THE PRODUCT PURCHASED
	float price;                                       // PRICE OF THE PURCHASED PRODUCT
};

  //////////////////////////////////////////////////////////////////////////////////////////////////
 //                                  STRUCTURE FOR CUSTOMER                                      //
//////////////////////////////////////////////////////////////////////////////////////////////////

struct cust
{
	char name[20];                                     // NAME OF THE CUSTOMER
    unsigned long long int phoneno;                    // PHONE NUMBER OF THE CUSTOMER
	product detail[20];                                // NESTED STRUCTURE OF PRODUCT
	float bill;
} c1;

  //////////////////////////////////////////////////////////////////////////////////////////////////
 //                             FUNCTION FOR CALCULATING BILL                                    //
//////////////////////////////////////////////////////////////////////////////////////////////////

float cal(cust c1)
{

	float n=0.0;
	int i;
	for(i=1;i<=c1.detail[i].quantity;i++)
	{
		n=n+(c1.bill+c1.detail[i].price*c1.detail[i].quantity);
	}
	return n;
};

  //////////////////////////////////////////////////////////////////////////////////////////////////
 //                                        MAIN FUNCTION                                         //
//////////////////////////////////////////////////////////////////////////////////////////////////

int main()
{
    clrscr();
    cust c1;
	int n, i;

  //////////////////////////////////////////////////////////////////////////////////////////////////
 //                             TAKINING INPUT OF THE CUSTOMER DETAILS                           //
//////////////////////////////////////////////////////////////////////////////////////////////////

	printf("\n\t\t\tEnter Customer details : ");
	printf("\nEnter Customer name : ");
	scanf("%s", &c1.name);
	printf("\nEnter Customer Phone Number : ");
	scanf("%llu", &c1.phoneno);

  //////////////////////////////////////////////////////////////////////////////////////////////////
 //                             TAKINING INPUT OF THE PRODUCT DETAILS                            //
//////////////////////////////////////////////////////////////////////////////////////////////////

	printf("\n\n\t\t\tEnter product details : ");
	printf("\nEnter number of products purchased : ");
	scanf("%d", &n);
	for(i=1; i<=n; i++)
	{
		printf("\nEnter product name : ");
		scanf("%s", &c1.detail[i].productname);
		printf("\nEnter quantity : ");
		scanf("%d", &c1.detail[i].quantity);
		printf("\nEnter price : ");
		scanf("%f", &c1.detail[i].price);
	}
	c1.bill=cal(c1);

  //////////////////////////////////////////////////////////////////////////////////////////////////
 //                                       DISPLAYING BILL                                        //
//////////////////////////////////////////////////////////////////////////////////////////////////

	clrscr();
    printf("\n\n....................................BILL...................................");
	printf("\n\nCUSTOMER NAME : %s", c1.name);
	printf("\n\nCUSTOMER PHONE NO : %llu", c1.phoneno);
	printf("\n\n\t\t\tCASH MEMO : ");
	printf("\nSr.No\tProduct name\t\t\tQuantity\tPrice");
	for(i=1;i<=n;i++)
	{
		printf("\n%d \t%s \t\t\t\t%d \t\t%f", i, c1.detail[i].productname, c1.detail[i].quantity, c1.detail[i].price);
	}
	printf("\n\n\t\t\t\t\t Total Amount : %f", c1.bill);
	getch();
	return 0;
}

  //////////////////////////////////////////////////////////////////////////////////////////////////
 //                                     END OF THE PROGRAM                                       //
//////////////////////////////////////////////////////////////////////////////////////////////////

     ///////////////////////////////////////////////////////////////////////////////////////////////
    //                                           REFERENCE                                       //
   //               --> Programming in ANSIC by E Balagurusamy Fourth Edition                   //
  //                --> Deitel  and Deitel, C How to Program                                   //
 //                 --> Dey and Ghosh, Programming in C, Oxford University Press              //
///////////////////////////////////////////////////////////////////////////////////////////////
