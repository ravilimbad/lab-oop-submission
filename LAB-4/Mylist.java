import java.util.Scanner;

class List
{
	private
		int n,i=0,sum=0,mean;
		int []array;
		int []count;
	
	public
		void size(int n)
		{
			array=new int[n];
			count=new int[n];

		}
		
		void add(int x)
		{	if(i<n)
			{	array[i]=x;
				i++;
				System.out.println("\nNumber Has Been Enter");
			}
			else
				System.out.println("\nSize if Full");
		}

		void del()
		{	if(i>-1)
			{	int z;
				z=array[i];
				i--;
				System.out.println("\nNumber Has Been Deleted");
			}
			else
				System.out.println("\nCan Not Delet More Numbers");
		}

		void fre()
		{	for(int j=0;j< array.length ;j++)
				for(int k=(j+1);k<array.length;k++)
				{	if(array[j]==array[k])
						System.out.println("\nThe List Contains A Duplicate Element Of:"+j);
					else
						System.out.println("\nThe List Do Not Contain A Duplicate Element");
				}
		}

		void dup()
		{	for(int j=0;j<array.length;j++)
				for(int k=(j+1);k<array.length;k++)
				{	if(array[j]==array[k])
						for(int l=k;l<(array.length-1);l++)
						{	System.out.println("\nThe Duplicate Element Of "+l+" has Been Deleted");
							array[l]=array[l+1];
							i--;
						}
					else
						System.out.println("\nThe List Do Not Contain A Duplicate Element");
				}
		}

		void asc()
		{	int temp;
			for (int j=0 ; j<(array.length-1) ; j++)
			{
				for (int k=0 ; k<(array.length-1) ; k++)
				{
					if (array[k+1]<array[k])
					{
						temp = array[k];
						array[k] = array[k + 1];
						array[k + 1] = temp;
					}
				}
			}
			System.out.println("\nAscending order: ");
			for (int l=0 ; l<array.length ; l++)
				System.out.println(" "+array[l]+" ");
		}

		void dec()
		{	int temp;
			for (int j=0 ; j<(array.length-1) ; j++)
			{
				for (int k=0 ; k<(array.length-1) ; k++)
				{
					if (array[k+1]>array[k])
					{
						temp = array[k];
						array[k] = array[k + 1];
						array[k + 1] = temp;
					}
				}
			}
			System.out.println("\nDescending order: ");
			for (int l=0 ; l<array.length ; l++)
				System.out.println(" "+array[l]+" ");
		}

		void res()
		{	int temp;
			for(int j=0,k=(array.length-1);i<(array.length/2);j++,k--)
			{	temp=array[j];
				array[j]=array[k];
				array[k]=temp;
			}
			System.out.println("\nReverse order: ");
			for (int l=0 ; l<array.length ; l++)
				System.out.println(" "+array[l]+" ");
		}

		void maxi()
		{	int max=array[0];
			for(int j=1;j<array.length;j++)
				if(max<array[j])
					max=array[j];
			System.out.println("Max Number In The List Is:"+max);
		}
				
		void mini()
		{	int min=array[0];									
			for(int j=1;j<array.length;j++)
				if(min<array[j])
					min=array[j];
			System.out.println("Min Number In The List Is:"+min);
		}		

		void su()
		{	int sum1=0;
			for(int j=1;j<array.length;j++)
				sum1=sum1+array[j];
			System.out.println("Sumation:"+sum);
			sum=sum1;
		}

		void mea()
		{	mean=sum/array.length;
			System.out.println("Mean:"+mean);
		}

		void med()
		{	int middle = array.length/2;
			if (array.length%2 == 1)
		      	System.out.println("Meadian:"+array[middle]);
			else
		        System.out.println("Meadian:"+((array[middle-1] + array[middle]) / 2));
		}
	
		void mode()
		{	int z,l=0,max1;
			for(int j=0;j<array.length;j++)
			{	z=array[j];
				count[z]=count[z]+1;
			}
			max1=count[0];
		      for(int k=1;k<array.length;k++)
				if(max1<count[k])
		        		l=k;
			System.out.println("Mode:"+array[l]);
		}

		void sta()
		{	int sum1=0;
			for (int j=0;j<array.length;j++)
			sum1=sum1+((array[j]-mean)*(array[j]-mean));
			double variance =(double)(sum1/array.length);
			double standardDeviation = Math.sqrt(variance);
		}
	
};

class Mylist
{	public static void main(String[]args)
	{
		List l1;
		int n, ch, x;
		System.out.println("Enter How Many Number You Want To Enter:");
 		Scanner in =new Scanner(System.in);
	 	n=in.nextInt();
		l1.size(n);
	 	System.out.println("\n");
		
		do{
	 			System.out.println("\n\t1.Add an number in the list");
	 			System.out.println("\n\t2.Delete an item from the list");
				System.out.println("\n\t3.Show frequency");
				System.out.println("\n\t4.Remove duplicate values");
				System.out.println("\n\t5.Sort the list in ascending order");
				System.out.println("\n\t6.Sort the list in descending order");
				System.out.println("\n\t7.Reverse the list");
				System.out.println("\n\t8.Max number in the list");
				System.out.println("\n\t9.Max number in the list");
				System.out.println("\n\t10.Calculate sum of the list");
				System.out.println("\n\t11.Calculate mean of the list");
				System.out.println("\n\t12.Calculate median of the list");
				System.out.println("\n\t13.Calculate mode of the list");
				System.out.println("\n\t14.Calculate standard deviation of the list");
				System.out.println("\n\t15.Exit");
	 			System.out.println("\n\tEnter Your Choice:");
	 			ch=in.nextInt();
	 			switch(ch)
				{
					case 1:
						System.out.println("\nEnter The Number");
						x=in.nextInt();
						l1.add(x);
						break;
					case 2:
						l1.del();
						break;
					case 3:
						l1.fre();
						break;
					case 4:
						l1.dup();
						break;
					case 5:
						l1.asc();
						break;
					case 6:
						l1.dec();
						break;
					case 7:
						l1.res();
						break;
					case 8:
						l1.maxi();
						break;
					case 9:
						l1.mini();
						break;
					case 10:
						l1.su();
						break;
					case 11:
						l1.mea();
						break;
					case 12:
						l1.med();
						break;
					case 13:
						l1.mode();
						break;
					case 14:
						l1.sta();
						break;
				}

	 		}while(ch<15);
	}
}

