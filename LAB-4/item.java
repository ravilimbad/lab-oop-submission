import java.util.Scanner;

// Made by:- Ravi Limbad 
// Roll no:- 1401077
// list class begins 
class list
{
	private
                // data members
		int no;
		int price;
		static int count=0;
	public 
                // member functions
		void setno(int n)
			{
    				 no=n;                          // enter item no
			}
		void setprice(int p)
			{
   				 price=p;                       // enter item price
			}
		int getno()
			{
   				return no;
			}

		int getprice()
			{ 
 				return price;
 
			}
        // contructor
        list()
	{
 		count++;
 		System.out.println("From constructor object change in static count ="+count);
	}
}//list class ends

class item
{
	public static void main(String[]args)

	{
 	int n,i,j,r;
	int item_no,item_price;
	System.out.println("Enter How Many Items You want->");
 	Scanner input =new Scanner(System.in);
 	n=input.nextInt();
	
	list[] array=new list[n];

/* here the size of object array is initialized to n but still we have to initiaize each object independently */

for ( i=1; i<array.length; i++) 
	{
		array[i]=new list();
	}
//Now there are n objects of Item class
 	
for(j=1;j<array.length;j++)//i and j are indecies, you can use anyone.
	{

 		System.out.println("Enter Item No->");
 		item_no=input.nextInt();
 		array[j].setno(item_no);
 
 		System.out.println("Enter Item Price->");
 		item_price=input.nextInt();
 		array[j].setprice(item_price);
  	}
  	for (r=1;r<array.length;r++)
 	 {
  		System.out.println("Item No" +r +"="+array[r].getno());
  		System.out.println("Item Price" +r+ "="+array[r].getprice());
 		System.out.println("From class calling of static count ="+list.count);
 	 } 

	}// main function ends 
}//end of main class
