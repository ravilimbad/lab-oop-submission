   //////////////////////////////////////////////////////////////////////////////////////////////////
  //                                     MADE BY:- RAVI LIMBAD                                    //
 //                                        ROLL NO:- 1401077                                     // 
//////////////////////////////////////////////////////////////////////////////////////////////////


  //////////////////////////////////////////////////////////////////////////////////////////////////
 //                                          HEADER FILE                                         //
//////////////////////////////////////////////////////////////////////////////////////////////////

#include<iostream>
#include<stdlib.h>
#include<conio.h>
using namespace std;

  //////////////////////////////////////////////////////////////////////////////////////////////////
 //                                  STRUCTURE FOR PRODUCT                                       //
//////////////////////////////////////////////////////////////////////////////////////////////////

struct product
{
	char productname[20];                              // NAME OF THE PRODUCT PURCHASED
	int quantity;                                      // QUANTITY OF THE PRODUCT PURCHASED  
	int price;                                       // PRICE OF THE PURCHASED PRODUCT
	
	product *pnext;                                    // POINTER   
}*phead, *ptail, *p, *ptemp;                           // HEAD, TAIL, TEMPORARY POINTER 

  //////////////////////////////////////////////////////////////////////////////////////////////////
 //                                  STRUCTURE FOR CUSTOMER                                      //
//////////////////////////////////////////////////////////////////////////////////////////////////

struct cust
{
	char name[20];                                     // NAME OF THE CUSTOMER
    unsigned long long int phoneno;                    // PHONE NUMBER OF THE CUSTOMER
	product detail[20];  
} c1;

  //////////////////////////////////////////////////////////////////////////////////////////////////
 //                                     INTODUCTION FUNCTION                                     //
//////////////////////////////////////////////////////////////////////////////////////////////////

void intro()
{
    cout<<"\n\n\t\t              RETAIL STORE.CPP";
    cout<<"\n\n\t\t           MADE BY : RAVI LIMBAD";
    cout<<"\n\n\t\t             ROLL NO : 1401077";
    cout<<"\n\n\t\t             enter to continue";
    getch();
}

  //////////////////////////////////////////////////////////////////////////////////////////////////
 //                                        MAIN FUNCTION                                         //
//////////////////////////////////////////////////////////////////////////////////////////////////

int main()
{	
    intro();
    system("cls");
	phead=ptail='\0';                                                // INITIALIZING HEAD AND TAIL POINTER TO NULL
	int n, i,total=0,amt=0;                                          // INITIALIZING VARIABLES - n is number of products purchased
	cout<<"\n\t\t\tEnter Customer details : ";
	cout<<"\nEnter Customer name : ";
	cin>>c1.name;
	cout<<"\nEnter Customer Phone Number : ";
	cin>>c1.phoneno;
	system("cls");
	cout<<"\n\n\t\t\tEnter product details : ";
	cout<<"\nEnter number of products purchased : ";
	cin>>n;
	for(i=1; i<=n; i++)
	{	p=new product;
		cout<<"\nEnter product name : ";
		cin>>p->productname;
		cout<<"\nEnter quantity : ";
		cin>>p->quantity;
		cout<<"\nEnter price : ";
		cin>>p->price;
		if(phead == '\0')
	    { 	phead=p;
	    	ptail=p;
		}
	    else
	    {  	ptail->pnext=p;	
			ptail=ptail->pnext;
		}
		ptail->pnext='\0';
		
	}
	system("cls");
	cout<<"\n\n....................................BILL...................................";
	cout<<"\n\nCUSTOMER NAME : "<<c1.name;
	cout<<"\n\nCUSTOMER PHONE NUMBER : "<<c1.phoneno;
	cout<<"\n\n\t\t\tCASH MEMO : ";
	cout<<"\nSr.No\tProduct name\t\t\tQuantity\tPrice";
	for(ptemp=phead,i=1;ptemp!='\0';ptemp=ptemp->pnext,i++)
    {      		amt=(ptemp->price)*(ptemp->quantity);
				cout<<"\n "<<i<<"\t "<<ptemp->productname<<"\t\t\t"<<ptemp->quantity<<"\t "<<ptemp->price;
				total+=amt;
				
	 }
	
	cout<<"\n\n\t\t\t\t\t Total Amount : "<<total;
	getch();
	return 0;
}

  //////////////////////////////////////////////////////////////////////////////////////////////////
 //                                     END OF THE PROGRAM                                       //
//////////////////////////////////////////////////////////////////////////////////////////////////


